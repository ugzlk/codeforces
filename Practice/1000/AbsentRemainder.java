import java.util.Scanner;

public class AbsentRemainder {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        for (int i = 0; i < t; i++) {
            int[] seq = new int[sc.nextInt()];
            for (int j = 0; j < seq.length; j++) {
                seq[j] = sc.nextInt();
            }
            int min = seq[0];
            for (int j = 1; j < seq.length; j++) {
                if (seq[j] < min) {
                    min = seq[j];
                }
            }
 
            int k = 0;
            int j = 0;
            while (k < seq.length/2) { 
                if (seq[j] != min) {
                    System.out.println(seq[j] + " " + min);
                    k++; 
                }
                j++;
            }
        }
        sc.close();
    }

    private static int binarySearch(int[] arr, int l, int r, int x) {
        if (r >= l) {
            int mid = l + (r - l)/2;
            
            if (arr[mid] == x)
                return mid;
            
            // Left subarray
            if (arr[mid] < x)
                return binarySearch(arr, l, mid-1, x);

            // Right subarray
            return binarySearch(arr, mid+1, r, x);
        }
        // Element not found
        return -1;
    }
}
