import java.util.Scanner;

public class EducationalGame1 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
       
        int n = sc.nextInt();
        if (n > 1) {
            // Store the number of moves needed for every i
            long[] moves = new long[n-1];
           
            long[] a = new long[n];

            for (int i = 0; i < n; i++) {
                a[i] = sc.nextLong();
            } 

            // Initialize dynamic programming array 
            moves[0] = a[0];
       
            // Typecast to truncate after decimal polong
            long t = (long) (Math.log(n-1) / Math.log(2));

            a[0+(int)Math.pow(2, t)] += a[0];

            for (int i = 1; i < n-1; i++) {
                moves[i] = moves[i-1] + a[i];
                t = (long) (Math.log(n-1-i) / Math.log(2));
                a[i + (int) Math.pow(2, t)] += a[i];
            }
      
            for (long m : moves) {
                System.out.println(m);
            } 
        }
        sc.close();
    }
}
