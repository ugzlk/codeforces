import java.util.Scanner;

public class KDivisibleSum {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int t = sc.nextInt();
        int[] maxArr = new int[t];
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int k = sc.nextInt();
        
            if (n > k & n % k != 0) {
                // The following formula brings k to the closest multiple that is greater or equal than n 
                k = n + k - n % k; 
            }
            else if (n > k & n % k == 0) {
                k = n;
            }

            int maxElement = (k % n == 0) ? k / n : k / n + 1;
            maxArr[i] = maxElement; 
        }
        sc.close();

        for (int i = 0; i < maxArr.length; i++) {
            System.out.println(maxArr[i]);
        }
    }
}
