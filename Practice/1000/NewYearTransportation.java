import java.util.Scanner;

public class NewYearTransportation {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int t = sc.nextInt();
        boolean reached = false;
 
        // n-1 portals for n cells 
        int[] portals = new int[n-1];
        for (int i = 0; i < n-1; i++) {
            portals[i] = sc.nextInt();
        }
        sc.close();

        int pos = 1;
        while (!reached & !(pos > n)) {
            // System.out.println("Aktuelle Position: " + pos);
            if (pos == t) {
                System.out.println("YES");
                reached = true;
                break;
            }
            
            if (pos >= n) {
                break;
            }
            // portals[pos-1] because portals array starts at 0
            pos = pos + portals[pos-1];
            
        }
        if (!reached) System.out.println("NO");  
    } 
}
