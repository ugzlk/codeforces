import java.util.Scanner;

public class PashaAndStick {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.close();
        
        if (n >= 6) {
            int r = n % 4;
            
            switch (r) {
                case 2:   System.out.println(n / 4);
                          break;
                case 0:   System.out.println(n / 4 - 1);
                          break;
                default:  System.out.println(0);
                          break; 
            }
        } else {
            System.out.println(0);
        }
    }
}
