import java.util.Scanner;

public class RoadsideTrees {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        
        // Consider first tree beforehand 
        int lissHeight = sc.nextInt();
        int time = lissHeight; 
        
        for (int i = 0; i < n-1; i++) {
            int nextTree = sc.nextInt();
            time += 1 + Math.abs(lissHeight - nextTree);
            lissHeight = nextTree; 
        }
        sc.close();
        System.out.println(time+n);
    } 
}
