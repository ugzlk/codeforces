import java.util.Scanner;
import java.util.ArrayList;

public class ShellGame {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int x = sc.nextInt();
        sc.close();

        // This is important for efficiency
        // The operators movements repeat after 6 consecutives movements
        // It s basically one pattern of 6 movements which is repeated 
        n = n % 6;

        ArrayList<Integer> shells = new ArrayList<Integer>(3);
        shells.add(0);
        shells.add(0);
        shells.add(0);
        shells.set(x, 1);
        
        // c helps us start with the right movement        
        int c = (n % 2 == 0) ? 0 : 1;
        // (c-1) * (c-1) so we subtract 1 when c is 0 and 0 when c is 1
        for (int i = c; i <= n - (c-1) * (c-1); i++) {
            int tmp;
            if (i % 2 != 0) {
                tmp = shells.get(0);
                shells.set(0, shells.get(1));
                shells.set(1, tmp); 
            } else {
                tmp = shells.get(2);
                shells.set(2, shells.get(1));
                shells.set(1, tmp); 
            } 
        }
        System.out.println(shells.indexOf(1));
    }
}
