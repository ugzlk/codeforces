import java.util.Scanner;

public class TheatreSquare {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int a = sc.nextInt();
        sc.close();

        // See how many times a fits in n and then in m in order to calculate the needed flagstones
        int AN = n % a == 0 ? n / a : n / a + 1;
        int AM = m % a == 0 ? m / a : m / a + 1;

        long product = Long.valueOf(AN) * Long.valueOf(AM);
        System.out.println(product);
    }
}
