import java.util.Scanner;

public class AllTheVowelsPlease2 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        float k = sc.nextInt();
        sc.close();

        // Find a divisor

        // Possible divisors must be at least 5
        float i = 5; 
        float j = 5; 
        
        if (k < 25) {System.out.println(-1);} else {
            while (j >= 5) {
                if (k % i == 0 & k / i >= 5) {
                    j = k / i;
                    break;
                } else {  
                    j = k / i;
                    i = i + 1; 
                }
            }
            
            if (j >= 5) {
                // Find vowelly word
                char[] vowels = {'a', 'e', 'i', 'o', 'u'};
                int index = 0;
                StringBuilder vowellySB = new StringBuilder();
                for (int p = 0; p < i; p++) {
                    index = p % 5; 

                    for (int q = 0; q < j; q++) {
                        vowellySB.append(vowels[index]);
                        index = (index + 1) % 5;
                    }
                }
                
                System.out.println(vowellySB.substring(0)); 
            } else {
                System.out.println(-1);
            }
        }
    }
}
