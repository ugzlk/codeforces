import java.util.Scanner;

public class CoatOfAnticubism {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int max = sc.nextInt();
        int sum = max;

        for (int i = 0; i < n-1; i++) {
            int next = sc.nextInt();
            if (next > max) {max = next;}
            sum += next; 
        }
        sc.close();
        System.out.println(max-sum+max+1);
    }
}
