import java.util.Scanner;
import java.util.HashMap;

public class Fruits {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        int[] prices = new int[n];
        HashMap<String, Integer> fruits = new HashMap<String, Integer>(n);

        for (int i = 0; i < n; i++) {
            prices[i] = sc.nextInt();
        }
        for (int i = 0; i < m; i++) {
            String fruit = sc.next();
            if (fruits.containsKey(fruit)) {
                fruits.put(fruit, fruits.get(fruit) + 1);
            } else {
                fruits.put(fruit, 1);
            }
        }
        sc.close();
        
        
        // Calculate minimal cost
        int minCost = 0;
        HashMap<String, Integer> dummyFruits = copyHashMap(fruits);
        int[] dummyPrices = copyArray(prices);
        for (int i = 0; i < fruits.size(); i++) {
            int minPrice = dummyPrices[0];
            int index = 0;
            for (int j = 1; j < dummyPrices.length; j++) {
                if (dummyPrices[j] < minPrice) {
                    minPrice = dummyPrices[j];
                    index = j;
                } 
            }
            dummyPrices[index] = 101;

            String maxFruit = getMaxFruit(dummyFruits);
            int maxFruitCount = dummyFruits.get(maxFruit);
            dummyFruits.remove(maxFruit);
            
            minCost += minPrice * maxFruitCount;
        }

        // Calculate maximal cost
        int maxCost = 0;
        HashMap<String, Integer> dummyFruitsPrime = copyHashMap(fruits);
        for (int i = 0; i < fruits.size(); i++) {
            int maxPrice = prices[0];
            int index = 0;
            for (int j = 1; j < prices.length; j++) {
                if (prices[j] > maxPrice) {
                    maxPrice = prices[j];
                    index = j;
                } 
            }
            prices[index] = 0; 

            String maxFruit = getMaxFruit(dummyFruitsPrime);
            int maxFruitCount = dummyFruitsPrime.get(maxFruit);
            dummyFruitsPrime.remove(maxFruit);

            maxCost += maxFruitCount * maxPrice;
        }
        System.out.println(minCost + " " + maxCost);
    }

    private static String getMaxFruit (HashMap<String, Integer> fruits) {
    
        String maxFruit = "devil fruit";
        
        for (String j : fruits.keySet()) {
            if (fruits.get(maxFruit) != null) { 
                if (fruits.get(j) > fruits.get(maxFruit)) {
                    maxFruit = j;
                }
            } else {
                maxFruit = j;
            }
        }
        return maxFruit;
    }

    private static void printArray (int[] arr) {
        String result = "";
        for (int i = 0; i < arr.length; i++) {
            result += " " + String.valueOf(arr[i]);
        }
        System.out.println(result);
    }

    private static HashMap<String, Integer> copyHashMap (HashMap<String, Integer> mapToCopy) {
        HashMap<String, Integer> copy = new HashMap<String, Integer>(mapToCopy.size());

        for (String s : mapToCopy.keySet()) {
            copy.put(s, mapToCopy.get(s));
        }
        return copy; 
    }

    private static int[] copyArray(int[] arrToCopy) {
        int[] copy = new int[arrToCopy.length];

        for (int i = 0; i < arrToCopy.length; i++) {
            copy[i] = arrToCopy[i];
        }
        return copy;
    }
} 
