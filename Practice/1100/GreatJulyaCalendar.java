import java.util.Scanner;
import java.util.Comparator;
import java.util.PriorityQueue;

public class GreatJulyaCalendar {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextLong();
        sc.close();
        if (n == 0) {
            System.out.println(0);
        } else {
            System.out.println(priorityMoves(n));
        }
    }
    
    // Leads to StackOverflowError, too many recursion calls for n > 36271 
    private static long recursiveMoves (String nr) {
        long l = Long.parseLong(nr);
        if (l < 10) {
            return 1;
        }
        int maxDigit = nr.charAt(0) - '0';
        for (int i = 1; i < nr.length(); i++) {
            if (nr.charAt(i) - '0' > maxDigit) {
                maxDigit = nr.charAt(i) - '0';
            }
        }
        return recursiveMoves(String.valueOf(l-maxDigit)) + 1;
    }
    
    // Too slow for large n
    private static long iterativeMoves (long l) {
        int moves = 0;
        while (l >= 10) {
            String nr = String.valueOf(l);
            int maxDigit = nr.charAt(0) - '0';
            for (int i = 1; i < nr.length(); i++) {
                if (nr.charAt(i)-'0' > maxDigit) {
                    maxDigit = nr.charAt(i) - '0';
                }
                if (maxDigit == 9) {
                    break;
                }
            }
            l -= maxDigit;
            moves++;
        }

        return moves+1; 
    }

    // Too slow
    private static long priorityMoves (long l) {
        long moves = 0;
        while (l >= 10) {
            String digits = String.valueOf(l);
            PriorityQueue<Integer> pQ = new PriorityQueue<Integer>(digits.length(), new ReversedComparator());
            for (int i = 0; i < digits.length(); i++) {
                pQ.add(Integer.parseInt(digits.substring(i, i+1)));
            }
            l -= pQ.peek();
            moves++;
        }
        return moves+1;
    }

    private static long dynamicMoves (long l) {
        long moves = 0;
        return moves+1;
    } 
}

class ReversedComparator implements Comparator<Integer> {
    public int compare (Integer i, Integer j) {
        return j.intValue() - i.intValue(); 
    }
}
