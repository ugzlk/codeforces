import java.util.Scanner;

public class LongJumps {
    public static void main (String[] main) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int[] maxScores = new int[t];

        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = sc.nextInt(); 
            }

            int[] scores = new int[n];
            int k = n - 1;

            while (k >= 0) {
                if (k + a[k] < n) {
                    scores[k] = a[k] + scores[k + a[k]]; 
                } else {
                    scores[k] = a[k];
                }
                k--;
            }
            
            // Find maxScore out of scores[]
            int maxScore = 0;
            for (int j = 0; j < scores.length; j++) {
                if (scores[j] > maxScore) {
                    maxScore = scores[j]; 
                }
            }

            maxScores[i] = maxScore;      
        }
        sc.close();

        for (int i = 0; i < maxScores.length; i++) {
            System.out.println(maxScores[i]);
        }
    }
}
