import java.util.Scanner;
import java.util.PriorityQueue;

public class SeatingArrangements {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int[] inconveniences = new int[t];
 
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            
            PriorityQueue<Integer> pQ = new PriorityQueue<Integer>(m);
            int inconvenience = 0;
            for (int j = 0; j < m; j++) {

                int[] arr = new int[j];
                int next = sc.nextInt();
                int brokeAt = -1;
                for (int p = 0; p < j; p++) {
                    int pInt = pQ.poll();
                    arr[p] = pInt;
                    if (pInt < next) {
                        inconvenience++;
                    } else {
                        brokeAt = p;
                        break;
                    } 
                }
                pQ.add(next);
                int limit = brokeAt == -1 ? arr.length : brokeAt + 1;
                for (int p = 0; p < limit; p++) {
                    pQ.add(arr[p]);
                }
                // Ready for GC
                arr = null;
            }
            inconveniences[i] = inconvenience;        
        }
        sc.close();

        for (int i = 0; i < inconveniences.length; i++) {
            System.out.println(inconveniences[i]);
        }
    }
}
