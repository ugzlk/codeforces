import java.util.Scanner;

public class Taxi {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        
        int taxis = 0;
        int three = 0;
        int two = 0;
        int one = 0;

        for (int i = 0; i < n; i++) {
            int group = sc.nextInt();

            switch (group) {
                case 1: one++; 
                        break;
                case 2: two++; 
                        break;
                case 3: three++; 
                        break;
                case 4: taxis++; 
                        break;
                default: System.out.println("FALSE INPUT");
                         System.exit(1);
            }
        }
        sc.close();

        if (three > one) {
            taxis += one;
            three -= one;
            one = 0;
        } else {
            taxis += three;
            one -= three;
            three = 0;
        }

        taxis += two / 2;
        two = (two % 2 == 0) ? 0 : 1;
        
        if (three != 0) {
            taxis += three + two;
        } else if (one != 0) {
            taxis += one / 4;
            switch (one % 4) {
                case 0: taxis += two;
                        break;
                case 1: case 2:
                        taxis++;
                        break;
                case 3: taxis += 1 + two;
                        break;
            }
        } else {
            taxis += two;
        }
    
             

        System.out.println(taxis);
    }
}
