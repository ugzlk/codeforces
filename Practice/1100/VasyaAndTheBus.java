import java.util.Scanner;

public class VasyaAndTheBus {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int grownups = sc.nextInt();
        int kids = sc.nextInt();
        sc.close();

        if (grownups == 0 && kids != 0) {
            System.out.println("Impossible");
        } else if (kids == 0) {
            System.out.println(grownups + " " + grownups);
        } else if (grownups >= kids && kids > 0) {
            System.out.println(grownups + " " + (grownups + kids - 1));
        } else if (grownups < kids) {
            System.out.println(kids  + " " + (grownups + kids - 1));
        } 
    }
}
