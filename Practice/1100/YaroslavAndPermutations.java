import java.util.Scanner;
import java.util.HashMap;

public class YaroslavAndPermutations {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
     
        // First integer takes the observed nextInt(), second integer counts all observations of the first integer 
        HashMap<Integer, Integer> observations = new HashMap<Integer, Integer>(n); 
        for (int i = 0; i < n; i++) {
            int next = sc.nextInt();      
            if (observations.containsKey(next)) {
                observations.put(next, observations.get(next) + 1);
            } else {
                observations.put(next, 1);
            }  
        }
        sc.close();
  
        int maxObservationsOfASingleInteger = 0;
        for (int c : observations.values()) {
            if (c > maxObservationsOfASingleInteger) {
                maxObservationsOfASingleInteger = c;
            }
        }

        boolean possible = (n % 2 == 0) ? maxObservationsOfASingleInteger <= n / 2 :  maxObservationsOfASingleInteger <= n / 2 + 1;
        String output = possible ? "YES" : "NO";
        System.out.println(output); 
    }
}
