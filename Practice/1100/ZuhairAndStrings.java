import java.util.Scanner;
import java.util.HashMap;

public class ZuhairAndStrings {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int k = sc.nextInt();
        String s = sc.next();
        sc.close();

        HashMap<String, Integer> substrings = new HashMap<String, Integer>();
        StringBuilder lastSubstring = new StringBuilder(""); 
        for (char c : s.toCharArray()) {
            if (lastSubstring.length() != 0) {
                if (c != lastSubstring.charAt(0)) {
                    lastSubstring.replace(0, lastSubstring.length(), String.valueOf(c));
                } else {
                    lastSubstring.append(c);
                }
            } else {lastSubstring.append(c);}

            if (lastSubstring.length() == k) {
                String substr = lastSubstring.substring(0);
                if (substrings.containsKey(substr)) {
                    substrings.put(substr, substrings.get(substr) + 1);
                } else {
                    substrings.put(substr, 1);
                }
                lastSubstring.delete(0, lastSubstring.length());
            }
        }

        // Find max value in substrings
        int level = 0;
        for (String str : substrings.keySet()) {
            if (substrings.get(str) > level) {
                level = substrings.get(str);
            }
        }

        System.out.println(level); 
    }
} 
