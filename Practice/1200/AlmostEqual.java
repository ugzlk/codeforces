import java.util.Scanner;

public class AlmostEqual {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.close();

        if (n % 2 == 0) {
            System.out.println("NO");
        } else {
            int[] arr = new int[2*n];
            int left = 1;
            int right = 2*n;

            for (int i = 0; i < n; i++) {
                int j = i;
                while (j < arr.length) {
                    if (i % 2 == 0) {
                        arr[j] = left;
                        left++; 
                    } else {
                        arr[j] = right;
                        right--;
                    }
                    j += n;
                } 
            }
            System.out.println("YES");
            for (int p : arr) {
                System.out.print(p + " ");
            }
        }
    }
}
