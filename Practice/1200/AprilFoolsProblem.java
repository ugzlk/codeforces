// 802M
import java.util.Scanner;
import java.util.PriorityQueue;

public class AprilFoolsProblem {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        PriorityQueue<Integer> p = new PriorityQueue<Integer>();

        for (int i = 0; i < n; i++) {
            p.add(sc.nextInt());
        }
        sc.close();
        int sum = p.poll();
        for (int i = 1; i < k; i++) {
            sum += p.poll();
        }
        System.out.println(sum);
    }
}
