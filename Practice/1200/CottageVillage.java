// 15A
import java.util.Scanner;
import java.util.HashMap;
import java.util.PriorityQueue;

public class CottageVillage {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        float t = sc.nextInt();

        PriorityQueue<Float> xQueue = new PriorityQueue<Float>(n);
        HashMap<Float, Float> houseMap = new HashMap<Float, Float>(n);

        for (int i = 0; i < n; i++) {
            float xi = sc.nextFloat();
            float ai = sc.nextFloat();
            
            xQueue.add(xi);
            houseMap.put(xi, ai);            
        }
        sc.close();

        // You can always place a house before the first and behind the last house
        int possiblePos = 2;

        float lastPos = xQueue.peek() + houseMap.get(xQueue.poll()) / 2; 
        for (int i = 1; i < n; i++) {
            float xi = xQueue.poll();

            if (xi - houseMap.get(xi) / 2 - lastPos >= t) {
                possiblePos++;
                if (xi - houseMap.get(xi) / 2 - lastPos > t) {
                    possiblePos++;
                }
            }
            lastPos = xi + houseMap.get(xi) / 2; 
        }

        System.out.println(possiblePos);
    }
}
