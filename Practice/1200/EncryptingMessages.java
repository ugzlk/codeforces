// 177D1
import java.util.Scanner;
import java.util.LinkedList;
import java.util.ListIterator;

public class EncryptingMessages {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int c = sc.nextInt();

        LinkedList<Integer> message = new LinkedList<Integer>();
        LinkedList<Integer> key = new LinkedList<Integer>();
   
        // Fill 
        for (int i = 0; i < n; i++) {
            message.add(sc.nextInt());
        }

        for (int i = 0; i < m; i++) {
            key.add(sc.nextInt());
        }
        sc.close();
    
        // Encrypt
        for (int i = 0; i < n-m+1; i++) {
            ListIterator<Integer> it = key.listIterator(0);
            for (int j = i; j < m+i; j++) {
                message.set(j, (message.get(j) + it.next()) % c);
            }        
        }

        // Print
        for (int i = 0; i < n; i++) {
            System.out.print(message.get(i) + " ");
        } 
    }
}
