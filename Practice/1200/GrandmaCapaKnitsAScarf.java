// 1582C
import java.util.Scanner;
import java.util.PriorityQueue;

public class GrandmaCapaKnitsAScarf {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int[] result = new int[t];

        for (int i = 0; i < t; i++) {
            int n = Integer.parseInt(sc.nextLine());
            char[] pattern = sc.nextLine().toCharArray();
        
            if (n > 1) {
                int rightStart = (n % 2 == 0) ? n / 2 : n / 2 + 1;
            
                int erased = 0;
                PriorityQueue<Integer> p = new PriorityQueue<Integer>(26);

                for (char c = 'a'; c <= 'z'; c++) {
                    int l = 0;
                    int r = n - 1;

                    while (l < r) {
                        // System.out.println(c + ", " + pattern[l]);
                        // System.out.println(c + ", " + pattern[r]);
                        if (pattern[l] != pattern[r]) {
                            if (pattern[l] == c) {
                                l++;
                                erased++; 
                            } else if (pattern[r] == c) {
                                r--;
                                erased++; 
                            } else {
                                erased = 1000000;
                                break;
                            } 
                        } else {
                            l++;
                            r--;
                        }
                    }
                    p.add(erased);
                    erased = 0;
                }
                //System.out.println(p);
                result[i] = p.peek();
                if (result[i] == 1000000) {result[i] = -1;}
            } else {
                result[i] = 0;
            }
        }
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        } 
        sc.close();
    }
}
