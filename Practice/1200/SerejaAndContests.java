// 401B
import java.util.Scanner;
import java.util.PriorityQueue;

public class SerejaAndContests {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        // Current round Sereja is in
        int x = sc.nextInt();
        // Rounds Sereja already took part in
        int k = sc.nextInt();

        if (k != 0) {
            PriorityQueue<Integer> takenRoundIDs = new PriorityQueue<Integer>(k*2);
            
            // Count all missedRounds, count +2 for Div1 rounds 
            int missedRounds = k;

            for (int i = 0; i < k; i++) {
                int div = sc.nextInt();
                if (div == 1) {
                    missedRounds++;
                    takenRoundIDs.add(sc.nextInt());
                    takenRoundIDs.add(sc.nextInt());
                } else {
                    takenRoundIDs.add(sc.nextInt());
                }
            }
           
            int len = x - takenRoundIDs.size() - 1; 
            PriorityQueue<Integer> missedRoundIDs = new PriorityQueue<Integer>(len+1);
            int takenRound = takenRoundIDs.poll();
            for (int i = 1; i < x; i++) {
                if (i == takenRound) {
                    if (!takenRoundIDs.isEmpty()) {
                        takenRound = takenRoundIDs.poll();
                    }
                    continue;
                } 
                missedRoundIDs.add(i);
            }


            // Calculate min
            int min = 0;

            if (!missedRoundIDs.isEmpty()) {
                int last = missedRoundIDs.poll();
                for (int i = 1; i < len+1; i++) {
                    if (missedRoundIDs.isEmpty()) {
                        min++;
                        break;
                    }

                    if (missedRoundIDs.peek() - last == 1) {
                        min++;
                        missedRoundIDs.poll();
                        if (missedRoundIDs.isEmpty()) {
                            break;
                        }
                        last = missedRoundIDs.poll(); 
                    } else {
                        min++;
                        last = missedRoundIDs.poll();
                    }
                }
            }  

            // Every unused identifier is a Div2 round
            int max = x - missedRounds - 1;
            System.out.println(min + " " + max);
        } else {
            System.out.println((x / 2)  + " " + (x-1));
        }
        sc.close();
    }
}
