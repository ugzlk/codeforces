import java.util.Scanner;

public class VanyaAndLanterns {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int l = sc.nextInt();
        int[] arr = new int[n];
  
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        // BubbleSort
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n-i-1; j++) {
                if (arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
  
        // Find maximum difference between two lanterns or between a lantern and the start/end point of the street
        double maxDiff = 0;
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i+1] - arr[i] > maxDiff) {
                maxDiff = arr[i+1] - arr[i];
            }  
        }
        double diffToStart = arr[0] - 0;
        double diffToEnd = l - arr[arr.length-1];
    
        double maxDiffTo = diffToStart > diffToEnd ? diffToStart : diffToEnd;
        double output = maxDiffTo > maxDiff / 2 ? maxDiffTo : maxDiff / 2;
        // Print out the maximum difference
        System.out.println(output); 
        sc.close();
    }
}
