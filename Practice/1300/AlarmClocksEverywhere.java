// 1155C
import java.util.Scanner;

public class AlarmClocksEverywhere {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextLong(); 
        }
 
        long y = arr[0]; 
        long g = -y + arr[1]; 

        for (int i = 2; i < n; i++) {
            g = gcd(g, arr[i] - arr[i-1]);
        }

        for (int i = 0; i < m; i++) {
            if (g % sc.nextLong() == 0) {
                System.out.println("YES");
                System.out.println(y + " " + (i+1));
                System.exit(0);
            }
        }
        sc.close();
        
        System.out.println("NO");
    }

    // greatest common divisor
    private static long gcd (long a, long b) {
        long lesser = a > b ? b : a;
        long greater = lesser == a ? b : a;
        long r = greater % lesser;

        while (r != 0) {
            long tmp = r;
            r = lesser % r;
            lesser = tmp;
        }
        
        return lesser; 
    }
}
