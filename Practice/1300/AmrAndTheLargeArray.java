// 558B
import java.util.Scanner;
import java.util.HashMap;

public class AmrAndTheLargeArray {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        sc.close();

        // Find beauty of entire array
        int beauty = 1;
        HashMap<Integer, Integer> occ = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer[]> subsegments = new HashMap<Integer, Integer[]>();
        for (int i = 0; i < a.length; i++) {
            if (occ.containsKey(a[i])) {
                int v = occ.get(a[i]) + 1;
                occ.put(a[i], v);
                Integer[] subseg = {subsegments.get(a[i])[0], i+1};
                subsegments.put(a[i], subseg);
                if (v > beauty) {
                    beauty = v; 
                }
            } else {
                occ.put(a[i], 1);
                Integer[] subseg = {i+1, i+1};
                subsegments.put(a[i], subseg);
            }
        }
        int minLen = n;
        int l = -1;
        int r = -1;
        for (int key : occ.keySet()) {
            if (occ.get(key) == beauty) {
                Integer[] sub = subsegments.get(key);
                if (sub[1] - sub[0] + 1 <= minLen) {
                    minLen = sub[1] - sub[0] + 1;
                    l = sub[0];
                    r = sub[1];
                    if (minLen == beauty) {
                        break;
                    } 
                }
            } 
        }
        System.out.println(l + " " + r); 
    }
}
