// 1618D
import java.util.Scanner;

public class ArrayAndOperations {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            sc.nextLine();
            String[] arr = sc.nextLine().split(" ");
            String[] a = sorted(arr);
            solve(a, n, k); 
        } 
        sc.close();
    }
    
    // Looked at tutorial solution 
    private static void solve (String[] a, int n, int k) {
        int score = 0;
        for (int i = 0; i < n - 2*k; i++) {
            score += Integer.parseInt(a[i]);
        }
        for (int i = 0; i < k; i++) {
            score += Integer.parseInt(a[n-2*k+i]) / Integer.parseInt(a[n-k+i]);
        }
        System.out.println(score); 
    }

    private static String[] sorted (String[] a) {
        // InsertionSort
        for (int i = 1; i < a.length; i++) {
            int x = Integer.parseInt(a[i]);
            int j = i - 1;

            while (j >= 0 && Integer.parseInt(a[j]) > x) {
                a[j+1] = a[j];
                j--;
            }
            a[j+1] = String.valueOf(x);
        }
        return a;
    }
}
