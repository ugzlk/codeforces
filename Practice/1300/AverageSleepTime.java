// 808B
import java.util.Scanner;

public class AverageSleepTime {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
     
        double sum = 0;
        for (int i = 0; i < n; i++) {
            if (i < k && i <= n - k) {
                sum += (i+1) * sc.nextDouble();
            } else if (k <= i && i <= n - k) {
                sum += k * sc.nextDouble();
            } else if (n - i <= n - k) {
                sum += (n-i) * sc.nextDouble();
            } else {
                sum += (n-k+1) * sc.nextDouble();
            }
        }
        sc.close();

        double c = n - k + 1;
        System.out.println(sum / c); 
    }
}
