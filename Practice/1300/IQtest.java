// 25A
import java.util.Scanner;
import java.util.ArrayList;

public class IQtest {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> even = new ArrayList<Integer>(2);
        ArrayList<Integer> odd = new ArrayList<Integer>(2);                

        for (int i = 1; i <= n; i++) {
            int next = sc.nextInt();
            boolean isEven = (next % 2 == 0) ? true : false;
 
            if (isEven) {
                even.add(i);
            } else {
                odd.add(i);
            }

            if (even.size() >= 2) {
                if (odd.size() > 0) {
                    System.out.println(odd.get(0));
                    break;
                }     
            } else if (odd.size() >= 2) {
                if (even.size() > 0) {
                    System.out.println(even.get(0));
                    break;
                } 
            }
           
            
        }
        sc.close();
    }
}
