// 611B
import java.util.Scanner;

// This was my fastest solution which is too slow
// V2 is the tutorial solution 
public class NewYearOldProperty {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        long a = sc.nextLong();
        long b = sc.nextLong();
        sc.close();
       
        long yearsWithOneZero = 0;
        int balance = 0;
        StringBuilder binaryYear = new StringBuilder(Long.toBinaryString(a-1));

        // Balance is 0 when binaryYear has exactly one zero
        // Balance is -1 when binaryYear has no zeros
        // Balance is [amount of zeros]-1 when binaryYear has too many zeros
        if (binaryYear.substring(0).matches("1*0{1}1*")) {
            balance = 0;
        } else {
            if (binaryYear.substring(0).matches("1*")) { 
                balance = -1;
            } else {
                int zeros = 0;
                for (char c : binaryYear.substring(0).toCharArray()) {
                    if (c == '0') zeros++;
                }
                balance = zeros - 1; 
            }
        }

        long tStart = System.currentTimeMillis();
        for (long k = a; k <= b; k++) {
            boolean carry = false;
            for (int i = binaryYear.length()-1; i >= 0; i--) {
                if (binaryYear.charAt(i) == '0') {
                    binaryYear.setCharAt(i, '1');
                    carry = false;
                    balance--;
                    break;
                } else {
                    binaryYear.setCharAt(i, '0');
                    carry = true;
                    balance++; 
                }
            }

            if (carry) {
                binaryYear.append('1');
                binaryYear.reverse();
            }
           
            if (balance == 0) {
                yearsWithOneZero++;
            }
        }
        long tEnd = System.currentTimeMillis();
        System.out.println((tEnd - tStart) / 1000 + " seconds");
        System.out.println(yearsWithOneZero); 
    }
}
