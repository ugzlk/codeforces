// 611B
import java.util.Scanner;

// Tutorial
public class NewYearOldPropertyV2 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        long a = sc.nextLong();
        long b = sc.nextLong();
        sc.close();

        long yearsWithOneZero = 0;
      
        int i = 0;
        long lastPower = 0; 
        while (Math.pow(2, i) <= Math.pow(10, 19)) { 
            long power = (long) Math.pow(2, i)-1;
            if (power > a) {
                for (int j = i; j >= 0; j--) {
                    long s = power - (long) Math.pow(2, j);
                    if (s > lastPower && s >= a && s <= b) {
                        if (Long.toBinaryString(power - (long) Math.pow(2, j)).matches("1*0{1}1*")) { 
                            yearsWithOneZero++;
                        }
                    }
                }                
            }
            lastPower = power;
            i++;
        }

        System.out.println(yearsWithOneZero);
    }
}
