// 39F
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;

public class PacifistFrogs {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        // Hills
        int n = sc.nextInt();
        // Frogs
        int m = sc.nextInt();
        // Mosquitoes
        int k = sc.nextInt();

        HashMap<Integer, Integer> frogs = new HashMap<Integer, Integer>(m);

        for (int i = 1; i <= m; i++) {
            frogs.put(i, sc.nextInt());
        }

        int[] mosquitoes = new int[k];
        for (int i = 0; i < k; i++) {
            mosquitoes[i] = sc.nextInt();
        }
        sc.close();
        
        int minSmashedMosquitoes = 101; 
      
        HashMap<Integer, Integer> frogSmashMosquito = new HashMap<Integer, Integer>(m);
        for (int i = 1; i <= frogs.size(); i++) {
            int frog = frogs.get(i);
            
            int smashedMosquitoes = 0;
            for (int j = 0; j < mosquitoes.length; j++) {
                if (mosquitoes[j] % frog == 0) smashedMosquitoes++;
            }
            frogSmashMosquito.put(i, smashedMosquitoes);
            if (smashedMosquitoes < minSmashedMosquitoes) {
                minSmashedMosquitoes = smashedMosquitoes;
            } 
        }

        ArrayList<Integer> minFrogs = new ArrayList<Integer>();
        for (int i = 1; i <= frogSmashMosquito.size(); i++) {
            if (frogSmashMosquito.get(i) == minSmashedMosquitoes) {
               minFrogs.add(i); 
            }
        }

        System.out.println(minFrogs.size());
        for (Integer frog : minFrogs) {
            System.out.print(frog + " ");
        }
    }
}
