// 173A
import java.util.Scanner;

public class RockPaperScissors {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());

        String A = sc.nextLine();
        String B = sc.nextLine();
        int len = A.length() * B.length(); 
        int redSpotsA = 0;
        int redSpotsB = 0;


        if (n <= len) {
            int[] spots = RPS(A, B, redSpotsA, redSpotsB, n);
            redSpotsA = spots[0];
            redSpotsB = spots[1];
            System.out.println(redSpotsA + " " + redSpotsB); 
        } else {
            int[] spots = RPS(A, B, redSpotsA, redSpotsB, len);
            redSpotsA = spots[0];
            redSpotsB = spots[1]; 
            int m = n / len; 

            redSpotsA *= m;
            redSpotsB *= m;

            int r = n % len;
            spots = RPS(A, B, redSpotsA, redSpotsB, r);          
            redSpotsA = spots[0];
            redSpotsB = spots[1]; 
            System.out.println(redSpotsA + " " + redSpotsB); 
        }        
    }

    private static int[] RPS (String a, String b, int spotsA, int spotsB, int n) {
        int[] result = new int[2];

        if (n <= a.length() * b.length()) {
            for (int i = 0; i < n; i++) {
                char itemA = a.charAt(i % a.length());
                char itemB = b.charAt(i % b.length());

                switch (itemA) {
                    case 'R':
                        if (itemB == 'S') {
                           spotsB++; 
                        } else if (itemB == 'P') {spotsA++;}
                        break;
                    case 'P':
                        if (itemB == 'R') {
                           spotsB++; 
                        } else if (itemB == 'S') {spotsA++;}
                        break;
                    case 'S':
                        if (itemB == 'P') {
                            spotsB++; 
                        } else if (itemB == 'R') {spotsA++;}
                        break;
                    default: break;
                }
            }
        }
        result[0] = spotsA;
        result[1] = spotsB;
        return result;
    }
}
