// 413B
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;

public class SpykeChatting {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int employees = sc.nextInt();
        int chats = sc.nextInt();
        int logEvents = sc.nextInt();
        sc.nextLine();
 
        // matrix n * m of zeros and ones
        HashMap<Integer, ArrayList<Integer>> pMap = new HashMap<Integer, ArrayList<Integer>>(employees);
        for (int i = 0; i < employees; i++) {
            ArrayList<Integer> activeChats = new ArrayList<Integer>();
            for (int j = 0; j < chats; j++) {
                if (sc.nextInt() > 0) {
                    activeChats.add(j); 
                }
            }
            pMap.put(i, activeChats);
        }  

        // k lines description of log events
        int[] notifications = new int[pMap.size()];
        int[] chatMessages = new int[chats];
        int[] messageByEmployee = new int[pMap.size()];

        for (int i = 0; i < logEvents; i++) {
                int employee = sc.nextInt()-1;
                int chat = sc.nextInt()-1;

                chatMessages[chat] += 1;
                messageByEmployee[employee] += 1;
        }

        for (int i = 0; i < pMap.size(); i++) {
            for (int c : pMap.get(i)) {
                notifications[i] += chatMessages[c];
            } 
            notifications[i] -= messageByEmployee[i]; 
        }
        sc.close();
     
        for (int i = 0; i < notifications.length; i++) {
            System.out.print(notifications[i] + " ");
        }
    }
}
