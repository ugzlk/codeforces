// 57A
import java.util.*;

public class SquareEarth {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int x1 = in.nextInt(), y1 = in.nextInt(), x2 = in.nextInt(), y2 = in.nextInt();
		in.close();

		// P1 and P2 lie on opposite sides
		if (Math.abs(x1-x2) == n) {
			int diff1 = Math.abs(y1-n)+Math.abs(y2-n);
			int diff2 = y1+y2;
			if (diff1 >= diff2) {
				System.out.println(n+diff2);
			} else {
				System.out.println(n+diff1);
			}
		} else if (Math.abs(y1-y2) == n) {
			int diff1 = Math.abs(x1-n)+Math.abs(x2-n);
			int diff2 = x1+x2;
			if (diff1 >= diff2) {
				System.out.println(n+diff2);
			} else {
				System.out.println(n+diff1);
			}
			
		} else {
		// P1 and P2 lie on adjacent sides or the same side
			System.out.println(Math.abs(x1-x2) + Math.abs(y1-y2));
		}	
	}
}
