// 807B
import java.util.Scanner;

public class TShirtHunt {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int p = sc.nextInt();
        // Current score
        int x = sc.nextInt();
        // Score sufficient for winning
        int y = sc.nextInt();
        sc.close();

        if (x > y) {
            int tmp = x;
            while (tmp >= y) {
                if (gotATShirt(tmp, p)) {
                    System.out.println(0);
                    System.exit(0);
                } else {
                    tmp -= 50;
                }
            }
            System.out.println(minHacks(x, p)); 
        } else {
            System.out.println(minHacks(x, p));
        }
    }

    private static int minHacks (int s, int place) {
        int tmp = s;
        while (!gotATShirt(tmp, place)) {
            tmp += 50; 
        }

        int diff = tmp - s;
        return (diff + 50) / 100; 
    }

    private static boolean gotATShirt (int s, int place) {
        int k = (s / 50) % 475;
        for (int i = 0; i < 25; i++) {
            k = (k * 96 + 42) % 475;
            if (26 + k == place) {
                return true;
            }
        }
        return false;
    }
}
