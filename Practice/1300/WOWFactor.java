// 1178B
import java.util.Scanner;
import java.util.HashMap;

public class WOWFactor {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        sc.close();

        // Tutorial solution
        long a = 0;
        long b = 0;
        long c = 0;

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == 'o') {
                b += a;
            } else if (i > 0 && input.charAt(i-1) == 'v') {
                a++;
                c += b; 
            }
        }

        System.out.println(c); 
    }

    // Too slow
    private static int iterativeWOWs (String input) {
        // Stores every chunk of v s, chunks of v s are seperated by chunks of o s
        // First Integer: Index
        // Second Integer: Length 
        HashMap<Integer, Integer> V = new HashMap<Integer, Integer>();
        // Chunks of o s  
        HashMap<Integer, Integer> O = new HashMap<Integer, Integer>();       
        int chunkIndexV = 0;
        int chunkIndexO = 0;   
 
        char last = input.charAt(0);

        // If input string starts with o s dont consider them, they are irrelevant
        if (last == 'o') chunkIndexO = -1;

        int chunkSize = 1;
        for (int i = 1; i < input.length(); i++) {
            char next = input.charAt(i);
                
            if (last == 'v') {
                if (next == 'v') {
                    chunkSize++;
                    if (i == input.length() - 1) {
                        V.put(chunkIndexV, chunkSize);
                    }
                } else {
                    V.put(chunkIndexV, chunkSize);
                    chunkSize = 1;
                    chunkIndexV++;
                }
            } else {
                if (next == 'o') {
                    chunkSize++;
                } else {
                    O.put(chunkIndexO, chunkSize);
                    chunkSize = 1;
                    chunkIndexO++;
                }
            }
            last = next;    
        }

        int WOWs = 0; 
        for (int i = 0; i < V.size() - 1; i++) {
            int chunk = V.get(i);
            for (int j = i + 1; j < V.size(); j++) {
                int nextChunk = V.get(j);
                int localWOWs = (chunk - 1) * (nextChunk - 1);
                int OsInBetween = 0;
                for (int o = i; o < j; o++) {
                    OsInBetween += O.get(o);
                }
                WOWs += localWOWs * OsInBetween; 
            } 
        }
        return WOWs; 
    } 
}
