// 621B
import java.util.Scanner;
import java.util.HashMap;

public class WetSharkAndBishop {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] bishops = new int[n][2];
        for (int i = 0; i < n; i++) {
            bishops[i][0] = sc.nextInt();
            bishops[i][1] = sc.nextInt();
        }
        sc.close();

        // Store how many bishops are on each diagonal
        HashMap<Integer, Integer> plusDiagonals = new HashMap<Integer, Integer>(2000-1);
        HashMap<Integer, Integer> minusDiagonals = new HashMap<Integer, Integer>(2000-1);

        for (int i = 2; i <= 2000; i++) {
            plusDiagonals.put(i, 0);
        }

        for (int i = -999; i <= 999; i++) {
            minusDiagonals.put(i, 0);
        }

        for (int i = 0; i < n; i++) {
            int x = bishops[i][0];
            int y = bishops[i][1];

            plusDiagonals.put(y+x, plusDiagonals.get(y+x)+1);
            minusDiagonals.put(y-x, minusDiagonals.get(y-x)+1);
        }

        int result = 0;
        for (int diagonal : plusDiagonals.keySet()) {
            // -1 so we can apply the Gauss equation
            int bishopsDiag = plusDiagonals.get(diagonal) - 1; 
            result += (bishopsDiag * (bishopsDiag + 1)) / 2;
        }

        for (int diagonal : minusDiagonals.keySet()) {
            int bishopsDiag = minusDiagonals.get(diagonal) - 1; 
            result += (bishopsDiag * (bishopsDiag + 1)) / 2;
        }
        System.out.println(result); 
    }
}
