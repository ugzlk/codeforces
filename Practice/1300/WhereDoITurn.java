// 227A
import java.util.Scanner;

public class WhereDoITurn {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        Point A = new Point(sc.nextLong(), sc.nextLong());
        Point B = new Point(sc.nextLong(), sc.nextLong());
        Point C = new Point(sc.nextLong(), sc.nextLong());
        sc.close();

        // position is 0 when B is on AC, +1 on left side, -1 on right side
        // Found on StackOverflow, posted by 'Eric Bainville'
	    // https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
        long position = (C.x - A.x) * (B.y - A.y) - (C.y - A.y) * (B.x - A.x);
        
        if (position == 0) {
            System.out.println("TOWARDS");
        } else if (position > 0) {
            System.out.println("RIGHT");
        } else { 
            System.out.println("LEFT");
        }
    }

    static class Point {
        long x;
        long y;

        Point (long px, long py) {
            x = px;
            y = py;
        }

        Point to (Point other) {
            return new Point(other.x - x, other.y - y);
        }
    }
}
