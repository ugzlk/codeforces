// 429A
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
 
// The following code is ugly and too slow
public class XORtree {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        HashMap<Integer, Node> nodes = new HashMap<Integer, Node>(n);
        nodes.put(1, new Node(1, 0, null));
        
        for (int i = 1; i < n; i++) {
            nodes.put(i+1, new Node(i+1, -1, null));
        }
 
        // nodeOrder serves the purpose of traversing the nodes in the right order
        // Sorting the nodes afterwards would be too slow 
        ArrayList<Integer> nodeOrder = new ArrayList<Integer>(n); 
        nodeOrder.add(1); 
        
        for (int i = 0; i < n-1; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();
            Node xNode = nodes.get(x);
            Node yNode = nodes.get(y);
            if (hasHeight(x, nodes)) {
                nodes.put(y, new Node(y, xNode.height + 1, xNode));
                nodeOrder.add(y); 
            } else if (hasHeight(y, nodes)) {
                nodes.put(x, new Node(x, yNode.height + 1, yNode));
                nodeOrder.add(x);
            } else {
                System.out.println("This won't work");
                System.exit(1); 
            } 
        }
        /*
        PriorityQueue<Node> pNodes = new PriorityQueue<Node>(n, new SortbyHeight());
        for (int key : nodes.keySet()) {
            pNodes.add(nodes.get(key));
        }
        /*
        for (int i = 1; i < n; i++) {
            System.out.println("N " + (i+1) + " Height: " + nodes.get(i+1).height + " Parent: " + nodes.get(i+1).parent.id);
        }
        */
        sc.nextLine(); 
        String initValues = sc.nextLine();
        String goalValues = sc.nextLine();
        sc.close();
        
        Scanner scInit = new Scanner(initValues);
        Scanner scGoal = new Scanner(goalValues);
 
        int[] init = new int[n];
        int[] goal = new int[n];
        for (int i = 0; i < n; i++) {
            init[i] = scInit.nextInt();
            goal[i] = scGoal.nextInt();
        }
        scInit.close();
        scGoal.close();
 
        // Compute minimum operations and picks 
        int moves = 0; 
        ArrayList<Integer> picks = new ArrayList<Integer>();
        
        for (int j : nodeOrder) {
            //System.out.println("Node k: " + k.id + " h: " + k.height);
            Node k = nodes.get(j); 
            int i = k.id - 1;
            Node newNode = new Node(k.id, k.height, k.parent);

            if (i > 0) {
                k.parent = nodes.get(k.parent.id);
                newNode.evenTimes = k.parent.evenTimes;
                newNode.oddTimes = k.parent.oddTimes;
                if (k.height % 2 == 0) {
                    init[i] = (init[i] + k.parent.evenTimes) % 2;   
                    if (init[i] != goal[i]) {
                        picks.add(k.id);
                        moves++;
                        newNode.evenTimes++;
                    }
                    nodes.put(k.id, newNode);
                } else {
                    init[i] = (init[i] + k.parent.oddTimes) % 2;    
                    if (init[i] != goal[i]) {
                        picks.add(k.id);
                        moves++;
                        newNode.oddTimes++;
                    } 
                    nodes.put(k.id, newNode); 
                }
            } else {
                if (init[i] != goal[i]) {
                    picks.add(k.id);
                    moves++;
                    newNode.evenTimes++;
                    nodes.put(k.id, newNode);
                }
            }
        }
        System.out.println(moves);
        for (int pick : picks) {
            System.out.println(pick);
        }

    }

    private static boolean hasHeight (int x, HashMap<Integer, Node> pNodes) {
        return pNodes.get(x).height > -1;
    }
}

class Node {
    int id;
    int height;
    int evenTimes;
    int oddTimes;
    Node parent;

    public Node (int identifier, int h, Node parentNode) {
        id = identifier;
        height = h;
        evenTimes = 0;
        oddTimes = 0;
        parent = parentNode;
    }
}

class SortbyHeight implements Comparator<Node> {
    public int compare(Node a, Node b) {
        return a.height - b.height;
    }
} 
