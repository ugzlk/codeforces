// 429A
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;

public class XORtreeV2 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] edges = new int[n-1][2];
        for (int i = 0; i < n-1; i++) {
            edges[i][0] = sc.nextInt();
            edges[i][1] = sc.nextInt();
        }
        sc.nextLine(); 
        String[] init = sc.nextLine().split(" ");
        String[] goal = sc.nextLine().split(" ");
        sc.close();
        
        HashMap<Integer, Node> nodes = new HashMap<Integer, Node>(n);
        boolean rootFlip = Integer.parseInt(init[0]) != Integer.parseInt(goal[0]);
        nodes.put(1, new Node(0, rootFlip ? 1 : 0, 0));
      
        int operations = rootFlip ? 1 : 0;
        ArrayList<Integer> picks = new ArrayList<Integer>();
        if (rootFlip) picks.add(1);
 
        // This only works because the input of the edges is ordered, beginning at the root
        for (int i = 0; i < edges.length; i++) {
            int x = edges[i][0];
            int y = edges[i][1];
        
            int child = (nodes.containsKey(x)) ? y : x;
            int p = (child == x) ? y : x;    
        
            Node parent = nodes.get(p);
            int childHeight = parent.height + 1;
            int j = child - 1;
            int initVal = Integer.parseInt(init[j]);
            int goalVal = Integer.parseInt(goal[j]);
 
            if (childHeight % 2 == 0) {
                initVal = (initVal + parent.evenFlips) % 2;
                if (initVal != goalVal) {
                    operations++;
                    picks.add(child);
                    nodes.put(child, new Node(childHeight, parent.evenFlips+1, parent.oddFlips));
                } else {
                    nodes.put(child, new Node(childHeight, parent.evenFlips, parent.oddFlips));
                }
            } else {
                initVal = (initVal + parent.oddFlips) % 2;
                if (initVal != goalVal) {
                    operations++;
                    picks.add(child);
                    nodes.put(child, new Node(childHeight, parent.evenFlips, parent.oddFlips+1));
                } else {
                    nodes.put(child, new Node(childHeight, parent.evenFlips, parent.oddFlips));
                }
            }
        }
        System.out.println(operations);
        for (int pick : picks) {
            System.out.println(pick);
        } 
    }
}

class Node {
    int height;
    int evenFlips;
    int oddFlips;
    
    public Node (int pHeight, int pEvenFlips, int pOddFlips) {
        height = pHeight;
        evenFlips = pEvenFlips;
        oddFlips = pOddFlips;
    }
}
