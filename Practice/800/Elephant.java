import java.util.Scanner;

public class Elephant {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);   
        int input = sc.nextInt();
        sc.close();

        if (input % 5 != 0) {
            System.out.println(input / 5 + 1);
        } else {
            System.out.println(input / 5);
        } 
    }
}
