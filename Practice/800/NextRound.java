import java.util.Scanner;

public class NextRound {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
       
        int n = sc.nextInt();
        int k = sc.nextInt();
        int count = 0;
        int minScore = 1; 
        for (int i = 1; i <= n; i++) {
            int nextScore = sc.nextInt();
            if (i == k & nextScore > 0) {
                minScore = nextScore; 
            }
            if (nextScore >= minScore) {
                count++;
            } 
        }
        System.out.println(count);
        sc.close();
    }
}
