import java.util.Scanner;

public class Team {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
      
        int n = scanner.nextInt();
       
        int counter = 0;
        int numberSolved = 0;

        // Start at i = 1 so we can check modulo 3 properly
        for (int i = 1; i <= 3*n; i++) {
            if (scanner.nextInt() == 1) {
                counter++;
                if (counter == 2) {
                    numberSolved++;    
                }  
            } 
            if (i % 3 == 0) {
                counter = 0;
            }              
        }
        scanner.close();
        System.out.println(numberSolved);
    }
}
