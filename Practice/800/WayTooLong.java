import java.util.Scanner;

public class WayTooLong {
	
	public static void main(String[]args) 
	{
		Scanner scanner = new Scanner(System.in);
		
		int n = scanner.nextInt();
		String[] words = new String[n];		
	
		for (int i = 0; i < n; i++) 
		{
			String word = scanner.next();
			words[i] = word;	
		}
		scanner.close();
	
		for (int i = 0; i < n; i++) 
		{
			String word = words[i];
				
			if (word.length() > 10) 
			{
				int length = word.length();	
				String output = word.charAt(0) + String.valueOf(length-2) + word.charAt(length-1);	
				System.out.println(output);
			} 
			else {
				System.out.println(word);	
			}
		}
	}
}
