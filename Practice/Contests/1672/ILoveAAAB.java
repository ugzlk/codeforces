// 1672B 
import java.util.*;

public class ILoveAAAB {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		in.nextLine();

		for (int i = 0; i < t; i++) {
			String s = in.nextLine();

			if (s.length() < 2 || s.charAt(0) == 'B' || s.charAt(s.length() - 1) != 'B') {
				System.out.println("NO");
				continue;
			}
			
			int c = 1;

			for (int j = 1; j < s.length(); j++) {
				if (s.charAt(j) == 'B') {
					c--; 
				} else if (s.charAt(j) == 'A') {
					c++;
				}
				
				if (c < 0) {
					break;
				}
			} 

			if (c >= 0) System.out.println("YES");
			else System.out.println("NO");
		}
		in.close();
	}
}
