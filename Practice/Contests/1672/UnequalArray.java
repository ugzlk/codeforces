// 1672C
import java.util.*;

public class UnequalArray {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);

		int t = in.nextInt();

		for (int i = 0; i < t; i++) {
			int n = in.nextInt();
			int maxDistanceBetweenTwoPairs = 0;

			int indexOfFirstPair = -1;
			int indexOfSecondPair = -1;
			int prev = in.nextInt();
			for (int j = 1; j < n; j++) {
				int next = in.nextInt();
				if (indexOfFirstPair == -1 && prev == next) {
					indexOfFirstPair = j;
				} else if (prev == next) {
					indexOfSecondPair = j-1;
				}
				prev = next;	
			}
		

			if (indexOfSecondPair > 0) {
				maxDistanceBetweenTwoPairs = indexOfSecondPair - indexOfFirstPair;
				if (maxDistanceBetweenTwoPairs == 0) {
					maxDistanceBetweenTwoPairs++;
				}	
			}

			System.out.println(maxDistanceBetweenTwoPairs);
		}
		in.close();
	}
}
